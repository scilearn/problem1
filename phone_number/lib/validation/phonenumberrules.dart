class CheckPhoneNumber {
  String myNumber;
  static final String INVALIDFORMAT =
      'Error: Mobile Number must be in valid format'; //Error messages
  static final String INVALIDLENGHT =
      'Error: Mobile Number must have 10 digits'; //Error messages

  CheckPhoneNumber(String myNumber) {
    this.myNumber = myNumber;
    this.myNumber = _status();
  }

  String _status() {
    // provide form validation status to the form
    String response =
        _isFormatValid(_isLengthValid(_removeLongDistancedPrefex(myNumber)));
    return response;
  }

  // private methods used for validation
  String _isFormatValid(phoneNumber) {
    // only numbers are acceptable
    String value = phoneNumber;
    String phonePattern =
        r'(^[0-9]{10}$)'; // matches 10 digests from the start of the string
    RegExp regExp = new RegExp(phonePattern);
    //pre check for error condition

    if (!regExp.hasMatch(value)) {
      throw ArgumentError(INVALIDFORMAT);
    } else {
      return (value);
    }
  }

  String _removeLongDistancedPrefex(phoneNumber) {
    // trim long distance prefix if valid
    String value = phoneNumber;
    if (value[0] == '1' && value.length == 11) {
      return value.substring(
        1,
      );
    } else {
      return value;
    }
  }

  String _isLengthValid(phoneNumber) {
    // verify string is 10 digits
    String value = phoneNumber;
    //pre check for error condition
    if (value.length != 10) {
      throw ArgumentError(INVALIDLENGHT);
    } else {
      return value;
    }
  }

  // requested public methods

  String number() {
    //return phone number if valid
    if (myNumber != null) return myNumber;
    return null;
  }

  String areaCode() {
    //return phone number if valid
    if (myNumber != null) return myNumber.substring(0, 3);
    return null;
  }

  String exchangeCode() {
    //return phone number if valid
    if (myNumber != null) return myNumber.substring(3, 6);
    return null;
  }

  String lineNumber() {
    if (myNumber != null)
      return myNumber.substring(
        6,
      );
    return null;
  }

  String pretty() {
    // return pretty formatted string
    String prettyNumber;
    if (myNumber != null) {
      prettyNumber =
          '(' + areaCode() + ') ' + exchangeCode() + '-' + lineNumber();
      return prettyNumber;
    }
    return null;
  }
}
