import 'package:flutter_test/flutter_test.dart';
import 'package:phone_number/validation/phonenumberrules.dart';

void main() {
  const String SHORTNUM = '425'; //bad
  const String PRETTYNUM = '(415) 867-5309';
  const String AERACODE = '415';
  const String EXCHANGECODE = '867';
  const String LINENUMBER = '5309';
  const String LDSHORTNUM = '1425'; //bad
  const String LONGNUM = '41586753090000'; //bad
  const String NUM = '4158675309'; //good
  const String LDNUM = '14158675309'; //good
  const String LDLONGNUM = '141586753090000'; //bad
  const String ALPHANUM = '41586753o9'; //bad
  const String SYMBOLNUM = '415-867530'; //bad
  const String ALPHALDLONGNUM = '141586753090oo0'; //bad
  const String ALPHASHORTNUM = 'a425'; //bad

  group('Valid Cases', () {
    test('10 Digit Number', () {
      expect(CheckPhoneNumber(NUM).number(), NUM);
    });

    test('1 Prefix 10 Digit Number', () {
      expect(CheckPhoneNumber(LDNUM).number(), NUM);
    });
  });

  group('Number Format', () {
    test('Alhpa numeric error', () {
      try {
        CheckPhoneNumber(ALPHANUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDFORMAT);
      }
    });

    test('Symbol numeric error', () {
      try {
        CheckPhoneNumber(SYMBOLNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDFORMAT);
      }
    });

    test('Alhpa short numeric error', () {
      // should not hit this condition

      try {
        CheckPhoneNumber(ALPHASHORTNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });

    test('Symbol long numeric error', () {
      // should not hit this condition
      try {
        CheckPhoneNumber(ALPHALDLONGNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });
  });

  group('Number length', () {
    test('Short number error', () {
      try {
        CheckPhoneNumber(SHORTNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });

    test('1 prefex Short number error', () {
      try {
        CheckPhoneNumber(LDSHORTNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });

    test('Long number error', () {
      try {
        CheckPhoneNumber(LONGNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });

    test('1 prefex Long number error', () {
      try {
        CheckPhoneNumber(LDLONGNUM);
      } on ArgumentError catch (e) {
        expect(e.message, CheckPhoneNumber.INVALIDLENGHT);
      }
    });
  });

  group('Requested Methods', () {
    test('return valid number', () {
      expect(CheckPhoneNumber(NUM).number(), NUM);
    });

    test('get areacode', () {
      expect(CheckPhoneNumber(NUM).areaCode(), AERACODE);
    });

    test('get exchange code', () {
      expect(CheckPhoneNumber(NUM).exchangeCode(), EXCHANGECODE);
    });

    test('get line number', () {
      expect(CheckPhoneNumber(NUM).lineNumber(), LINENUMBER);
    });

    test('pretty print', () {
      expect(CheckPhoneNumber(NUM).pretty(), PRETTYNUM);
    });
  });
}
