// This is a basic Flutter widget test.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:phone_number/main.dart';

void main() {
  const String TITLE = 'Phone Number Validation';
  const String HINTTEXT = 'Add Phone Number';
  const String BUTTONTEXT = 'Submit';
  const String EMPTYFIELD = 'Please enter a phone number';
  const String VALIDNUM = '15088675309';
  // Create Global Finders.
  final titleFinder = find.text(TITLE);
  final hintTextFinder = find.text(HINTTEXT);
  final buttonTextFinder = find.text(BUTTONTEXT);

  group('Home State', () {
    testWidgets('Default UI Test', (WidgetTester tester) async {
      // Build our app and trigger a frame.
      await tester.pumpWidget(MyApp());
      expect(titleFinder, findsOneWidget);
      expect(hintTextFinder, findsOneWidget);
      expect(buttonTextFinder, findsOneWidget);
      expect(find.byType(TextFormField), findsOneWidget);
    });
  });

  group('Messages', () {
    testWidgets('Check Validation String', (WidgetTester tester) async {
      final emptyFieldMessageFinder = find.text(EMPTYFIELD);
      await tester.pumpWidget(MyApp());
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(emptyFieldMessageFinder, findsOneWidget);
    });

    testWidgets('Check SnackBar String', (WidgetTester tester) async {
      const String PHONENUMBERVALID = 'Phone Number Valid';
      final numberValidTextFinder = find.text(PHONENUMBERVALID);
      await tester.pumpWidget(MyApp());
      await tester.enterText(find.byType(TextFormField), VALIDNUM);
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(numberValidTextFinder, findsOneWidget);
    });
  });
}
